#!/usr/bin/env python3.6

import tkinter

def closeWindow(window):
    window.destroy()

def launchWindow():
    window = tkinter.Tk(screenName="Bleurp", baseName=None, className='Tk', useTk=1)
    return window

def main():
    window = launchWindow()
    closeWindow(window)

if __name__=="__main__":
    main()

#
